*What?*

VSProjectSorter is a command line application that, well, sorts the file references inside the Microsoft Visual Studio project file.

*Why?*

At work, we have this project of a big Asp.net MVC web site and a team of 6 people concurrently checking in changes to it. Unless we
have a very big feature to implement, we work with two branches "development" and "main". Developer A adds some files to the project
and checks-in "development". Later, Developer B also adds some files to the project and checks-in, then Developer C... well you got it. 
The problem is that these changes are merged to "main" branch not necessarily at the same order as they were checked into development and 
when a conflict occurs with the csproj file it is very hard to tell what should be at main branch csproj file because the file references
are written in completely random order.

Using this application, we can sort the csproj file and that makes the merge or conflict resolution process a lot easier. That should be
done automatically by Visual Studio, but seems like they won't do it any time soon.

*Does it work?*

Yes, at least for VS2012 project, but of course you will backup your csproj file before using this application, right?

*I loved it! Can I make any changes to it?*

Yes, please! 
One great improvement that maybe I'll do when I have some free time is to turn this into a Visual Studio add-in that runs everytime we
add a new file to the project.
