﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSProjectSorter
{
    class Program
    {
        static void Main(string[] args)
        {

            if (args.Length == 0)
            {
                Console.WriteLine("What is the project file name?");
                return;
            }

            var fileName = args[0];

            if (!File.Exists(fileName))
            {
                Console.WriteLine("Could not find file {0}!", fileName);
                return;
            }

            if (Path.GetExtension(fileName) != ".csproj" && Path.GetExtension(fileName) != ".sqlproj")
            {
                Console.WriteLine("File must have .csproj or .sqlproj extension!");
                return;
            }

            var sorter = new ProjectSorter();

            try
            {
                sorter.Sort(fileName);
                Console.WriteLine("Sorted file {0}.", fileName);
            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine("Could not sort the file! Please make sure that the file is writable or that you checked it out for edit.");
            }

        }
    }
}
