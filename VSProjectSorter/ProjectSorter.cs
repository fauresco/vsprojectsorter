﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace VSProjectSorter
{
    public class ProjectSorter
    {

        public void Sort(string fileName)
        {
            const string ns = @"{http://schemas.microsoft.com/developer/msbuild/2003}";

            var doc = XDocument.Load(fileName);

            var groups = doc.Elements().First().Descendants(ns + "ItemGroup").Where(
                i => i.Descendants(ns + "Compile").Any() || 
                     i.Descendants(ns + "Content").Any() || 
                     i.Descendants(ns + "None").Any() || 
                     i.Descendants(ns + "Build").Any() ||
                     i.Descendants(ns + "PostDeploy").Any() ||
                     i.Descendants(ns + "PreDeploy").Any() ||
                     i.Descendants(ns + "Reference").Any());

            foreach (var g in groups)
            {
                var sortedElements = g.Elements().OrderBy(e => e.Attributes("Include").First().Value).ToList();
                g.RemoveAll();
                g.Add(sortedElements);
            }

            doc.Save(fileName);
        }

    }
}
